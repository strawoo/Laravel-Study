<?php

use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::all();

        $users->each(function (\App\User $user) {
            $user->articles()->save(
                factory(\App\Article::class)->make()
            );
        });
    }
}
