<?php

namespace App\Http\Controllers;

use App\Documentation;
use Illuminate\Http\Request;

class DocsController extends Controller
{
    protected $docs;

    public function __construct(Documentation $docs)
    {
        $this->docs = $docs;
    }

    /**
     * @param $file
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function image($file)
    {
        $reqEtag = \Request::getETags();
        $genEtag = $this->docs->etag($file);

        if (isset($reqEtag[0])) {
            if ($reqEtag[0] === $genEtag) {
                return response('', 304);
            }
        }

        $image = $this->docs->image($file);

        return response($image->encode('png'), 200, [
            'Content-Type' => 'image/png',
            'Cache-Control' => 'public, max-age=0',
            'Etag' => $genEtag,
        ]);
    }

    /**
     * @param string $file
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(string $file = null)
    {
        $index = \Cache::remember('docs.index', 120, function () {
            return markdown($this->docs->get());
        });

        $content = \Cache::remember("docs.{$file}", 120, function () use ($file) {
            return markdown($this->docs->get($file ?: 'installation.md'));
        });

        return view('docs.show', compact('index', 'content'));
    }
}
