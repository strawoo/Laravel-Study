@extends('layouts.master')

@section('content')
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>
                <a href="{{ route('register') }}">Register</a>
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Laravel {{ $name or 'Welcome!' }} {{ $greeting or 'Hello' }}
        </div>

        <div class="links">
            <a href="https://laravel.com/docs">Documentation</a>
            <a href="https://laracasts.com">Laracasts</a>
            <a href="https://laravel-news.com">News</a>
            <a href="https://forge.laravel.com">Forge</a>
            <a href="https://github.com/laravel/laravel">GitHub</a>
        </div>

        <ul>
            @forelse($items as $item)
                <li>{{ $item }}</li>
            @empty
                <p>Empty</p>
            @endforelse
        </ul>
    </div>
    @include('partials.footer')
</div>
@endsection

@section('script')
    <script>
        console.log(1);
    </script>
@endsection
